# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# sensitive mode
RM_MODE ?= -s

log:
	mkdir -p $@

reference.fasta:
	zcat $(REFERENCE) >$@

table:
	$(foreach TABLE,$(TABLES), \
		$(shell ln -sf $(call get,$(TABLE),$(notdir $(PWD))) $@) \
	)

# NB: chr9_random:112122-120541_RLG
repeats.fasta:
	zcat $(DELETIONS) >$@

FRACTIONS_LST = hemizygous.lst not.hemizygous.lst other.lst
hemizygous.lst: table
	unhead <$< \
	| bawk '! /\#+,$$^/ { if ( $$8 == "Hemi" ) print $$1; }' >$@

not.hemizygous.lst: table
	unhead <$< \
	| bawk '! /\#+,$$^/ { if ( $$8 == "Normal" ) print $$1; }' >$@

other.lst: table
	unhead <$< \
	| bawk '! /\#+,$$^/ { if ( $$8 == "." ) print $$1; }' >$@


FRACTIONS_FASTA_GZ = $(FRACTIONS_LST:.lst=.lst.fasta.gz)
%.lst.fasta.gz: reference.fasta %.lst
	$(call load_modules); \
	ns-fasta-extract --list $^2 --input $< \
	| gzip -c >$@

%.tsv: %.lst.fasta.gz
	zcat $< \
	| fasta_length >$@

.META: contigs.stat
	1	sequences number
	2	mean
	3	median
	4	stdev
	5	min
	6	max
	7	total length

contigs.stat: $(FRACTIONS_FASTA_GZ)
	>$@; \
	TMP=$$(mktemp tmpXXX); \
	(for FILE in $^; do \
		zcat $$FILE \
		| fasta_length \
		| stat_base --total --min --max --mean --median --stdev --precision=10 2 \
		| sed "s/^/$$(zcat $$FILE | fasta_count -s)\t/" \
		| sed "s/^/$$FILE\t/"; \
	done) >"$$TMP"; \
	cat "$$TMP" \
	| bawk -v TOT=$$(stat_base --total 2 <"$$TMP") '!/[\#+,$$]/ { printf "%s\t%5.2f\n",$$0,$$2/TOT; }' \
	| select_columns 1 2 10 4 5 6 7 8 9 \
	| bsort -k1,1 >$@ \
	&& rm $$TMP


FRACTIONS_ORI_OUT = $(FRACTIONS_LST:.lst=.lst.fasta.ori.out)
# change to $(FRACTIONS_ORI_OUT) when multiple targets will be supported
.META: hemizygous.lst.fasta.ori.out
	1	Smith-Waterman score of the match
	2	% divergence = mismatches/(matches+mismatches)
	3	% of bases opposite a gap in the query sequence (deleted bp)
	4	% of bases opposite a gap in the repeat consensus (inserted bp)
	5	name of query sequence
	6	starting position of match in query sequence
	7	ending position of match in query sequence
	8	no. of bases in query sequence after the ending position of match
	9	C if match is with the Complement of the repeat consensus sequence, else +
	10	name of the matching interspersed repeat
	11	the class of the repeat
	12	no. of bases in (complement of) the repeat consensus sequence prior to beginning of the match (0 means that the match extended all the way to the end of the repeat consensus sequence)
	13	starting position of match in repeat consensus sequence
	14	ending position of match in repeat consensus sequence
	15	unique identifier for individual insertions 


# filter in order to reuse repeat masker results
%.lst.fasta.ori.out: %.tsv
	cat $(OLD_FRACTIONS_ORI_OUT) \
	| sed 's/^ *//' \
	| tr -s [:blank:] \\t \
	| filter_1col 5 <(cut -f1 $<) >$@

FRACTIONS_REPEATS_CLASS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.class)
%.lst.class: %.lst.fasta.ori.out
	$(call load_modules); \
	if [ -s $< ]; then \
		sed 's/^ *//' <$< \
		| tr -s [:blank:] \\t \
		| cut -f 10 \
		| bawk '!/^[$$,\#]/ { \
		split($$0,a,"_"); \
		print a[length(a)]; }' \
		| bsort \
		| uniq -c \
		| sed 's/^ *//' \
		| tr -s [:blank:] \\t \
		| bsort -k1,1nr \
		| select_columns 2 1 >$@; \
	else \
		touch $@; \
	fi;

FRACTIONS_REPEATS_STATS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.mask.stat)
%.lst.mask.stat: %.tsv %.lst.fasta.ori.out
	$(call load_modules); \
	buildSummary.pl -genome $< -useAbsoluteGenomeSize $^2 \
	| bawk 'BEGIN { tot_seq=0; tot_len=0; bp_masked=0; frac_masked=0; } !/[\#+,$$]/ { \
	if ( $$1 ~ /^Total Sequences/ ) { split($$1,a," "); print "total_sequences:", a[length(a)]; } \
	if ( $$1 ~ /^Total Length/ ) { split($$1,a," "); print "total_length (bp):", a[length(a)-1]; } \
	if ( $$1 ~ /^Total  / ) { split($$1,a," "); print "total_masquered (bp):", a[length(a)-1]; \
	print "total_masquered (%):", a[length(a)]; \
	exit } \   * avoid last lines *
	}' >$@


.META: %.lst.mask.class
	1	repeat class
	2	total masquered bases in contig fraction
	3	percentage of total fraction length

# extract the total bases of the contig set masquered by each repeat type.
# first extract the total bases of the contig fraction masquered by each repeat sequence
# then sum the masquered bases per repeat type
# then calculate the percentage by dividing for
# the total length of the contig fraction
FRACTIONS_MASQUERED_BY_REPEATS_STATS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.mask.class)
%.lst.mask.class: %.tsv %.lst.fasta.ori.out
	$(call load_modules); \
	TOTAL_LENGHT=$$(buildSummary.pl -genome $< -useAbsoluteGenomeSize $^2 \   * extract total contigs length *
	| bawk 'BEGIN { tot_seq=0; tot_len=0; bp_masked=0; frac_masked=0; } !/[\#+,$$]/ { \
	if ( $$1 ~ /^Total Length/ ) { split($$1,a," "); print a[length(a)-1]; exit; }}'); \
	\
	buildSummary.pl -genome $< -useAbsoluteGenomeSize $^2 \   * extract the total bases of the contig fraction masquered by each repeat sequence *
	| bawk '/={16}/{flag=1;next}/-{56}/{flag=0}flag' \
	| sed 's/^ *//' \
	| tr -s [:blank:] \\t \
	| bawk '!/^[$$,\#]/ { \
	split($$1,a,"_"); \
	print a[length(a)], $$3; }' \
	| bsort -k 1,1 \
	| bawk -v tot_len="$$TOTAL_LENGHT" 'BEGIN { masked=0; } \   * sum the masquered percentage by each repeat type *
	!/^[\#+,$$]/ { \
		if ( NR == 1 ) { start=$$1; masked +=$$2; } \
		else \
		{ \
			if ( $$1 != start ) { printf "%s\t%i\t%5.5f\n", start, masked, masked/tot_len; start=$$1; masked=0; } \
			masked +=$$2; \
		} \
	} \
	END { printf "%s\t%i\t%5.5f\n", start, masked, masked/tot_len; }' \
	| bsort -k 2,2nr >$@


mask.stat.all: contigs.stat $(FRACTIONS_REPEATS_STATS)
	(for FILE in $(FRACTIONS_REPEATS_STATS); do \
		paste <(echo -e "$$FILE") <(cat <$$FILE | cut -f2 | transpose | cut -f 4); \
	done) \
	| bsort -k 1,1 \
	| cat <(cut -f 1,3 $<) - \
	| sed '1s/^/\t$(notdir $(PWD))\n/' >$@

.PHONY:
test:


ALL +=  reference.fasta \
	repeats.fasta \
	contigs.stat \
	$(FRACTIONS_ORI_OUT) \
	$(FRACTIONS_REPEATS_CLASS) \
	$(FRACTIONS_REPEATS_STATS) \
	$(FRACTIONS_MASQUERED_BY_REPEATS_STATS) \
	mask.stat.all


INTERMEDIATE += 

CLEAN += log
