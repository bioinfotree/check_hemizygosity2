# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

STATS = $(addsuffix .stat.txt,$(call keys,TABLE))
%.stat.txt:
	for KEY in $(call keys,TABLE); do \
		ln -sf "../$$KEY/mask.stat.all" "$$KEY.stat.txt"; \
	done

stat.all.txt: $(STATS)
	paste $^ \
	| select_columns 1 2 4 6 8 10 >$@

.PHONY:
test:
	@echo $(STATS)


ALL += stat.all.txt

INTERMEDIATE += 

CLEAN +=
