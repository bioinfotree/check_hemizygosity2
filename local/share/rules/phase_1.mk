# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# sensitive mode
RM_MODE ?= -s

log:
	mkdir -p $@

reference.fasta:
	zcat $(REFERENCE) >$@

table:
	$(foreach TABLE,$(TABLES), \
		$(shell ln -sf $(call get,$(TABLE),$(notdir $(PWD))) $@) \
	)

# NB: chr9_random:112122-120541_RLG
repeats.fasta:
	zcat $(DELETIONS) >$@

FRACTIONS_LST = hemizygous.lst not.hemizygous.lst other.lst
hemizygous.lst: table
	unhead <$< \
	| bawk '! /\#+,$$^/ { if ( $$8 == "Hemi" ) print $$1; }' >$@

not.hemizygous.lst: table
	unhead <$< \
	| bawk '! /\#+,$$^/ { if ( $$8 == "Normal" ) print $$1; }' >$@

other.lst: table
	unhead <$< \
	| bawk '! /\#+,$$^/ { if ( $$8 == "." ) print $$1; }' >$@

FRACTIONS_FASTA_GZ = $(FRACTIONS_LST:.lst=.lst.fasta.gz)
%.lst.fasta.gz: reference.fasta %.lst
	$(call load_modules); \
	ns-fasta-extract --list $^2 --input $< \
	| gzip -c >$@


.META: contigs.stat
	1	sequences number
	2	mean
	3	median
	4	stdev
	5	min
	6	max
	7	total length

contigs.stat: $(FRACTIONS_FASTA_GZ)
	>$@; \
	TMP=$$(mktemp tmpXXX); \
	(for FILE in $^; do \
		zcat $$FILE \
		| fasta_length \
		| stat_base --total --min --max --mean --median --stdev --precision=10 2 \
		| sed "s/^/$$(zcat $$FILE | fasta_count -s)\t/" \
		| sed "s/^/$$FILE\t/"; \
	done) >"$$TMP"; \
	cat "$$TMP" \
	| bawk -v TOT=$$(stat_base --total 2 <"$$TMP") '!/[\#+,$$]/ { printf "%s\t%5.2f\n",$$0,$$2/TOT; }' \
	| select_columns 1 2 10 4 5 6 7 8 9 \
	| bsort -k1,1 >$@ \
	&& rm $$TMP


# query masked
FRACTIONS_MASKED = $(FRACTIONS_LST:.lst=.lst.fasta.masked)
# alignmentsq
FRACTIONS_CAT = $(FRACTIONS_LST:.lst=.lst.fasta.cat.gz)
# output with header and wildcards
FRACTIONS_ORI_OUT = $(FRACTIONS_LST:.lst=.lst.fasta.ori.out)
# output with no header or wildcards
FRACTIONS_OUT = $(FRACTIONS_LST:.lst=.lst.fasta.out)
# summary file
FRACTIONS_TBL = $(FRACTIONS_LST:.lst=.lst.fasta.tbl)

# change to $(FRACTIONS_ORI_OUT) when multiple targets will be supported
.META: hemizygous.lst.fasta.ori.out
	1	Smith-Waterman score of the match
	2	% divergence = mismatches/(matches+mismatches)
	3	% of bases opposite a gap in the query sequence (deleted bp)
	4	% of bases opposite a gap in the repeat consensus (inserted bp)
	5	name of query sequence
	6	starting position of match in query sequence
	7	ending position of match in query sequence
	8	no. of bases in query sequence after the ending position of match
	9	C if match is with the Complement of the repeat consensus sequence, else +
	10	name of the matching interspersed repeat
	11	the class of the repeat
	12	no. of bases in (complement of) the repeat consensus sequence prior to beginning of the match (0 means that the match extended all the way to the end of the repeat consensus sequence)
	13	starting position of match in repeat consensus sequence
	14	ending position of match in repeat consensus sequence
	15	unique identifier for individual insertions 

.PRECIOUS: $(FRACTIONS_ORI_OUT)
%.lst.fasta.ori.out: repeats.fasta %.lst.fasta.gz
	!threads
	$(call load_modules); \
	RepeatMasker \
	$(RM_MODE) \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $< \
	$^2

%.lst.fasta.masked %.lst.fasta.cat.gz %.lst.fasta.out %.lst.fasta.tbl: %.lst.fasta.ori.out
	touch $@

FRACTIONS_REPEATS_CLASS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.class)
%.lst.class: %.lst.fasta.ori.out
	$(call load_modules); \
	if [ -s $< ]; then \
		sed 's/^ *//' <$< \
		| tr -s [:blank:] \\t \
		| cut -f 10 \
		| bawk '!/^[$$,\#]/ { \
		split($$0,a,"_"); \
		print a[length(a)]; }' \
		| bsort \
		| uniq -c \
		| sed 's/^ *//' \
		| tr -s [:blank:] \\t \
		| bsort -k1,1nr \
		| select_columns 2 1 >$@; \
	else \
		touch $@; \
	fi;

FRACTIONS_REPEATS_STATS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.mask.stat)
%.lst.mask.stat: %.lst.fasta.tbl
	$(call load_modules); \
	bawk 'BEGIN { tot_seq=0; tot_len=0; bp_masked=0; frac_masked=0; } !/[\#+,$$]/ { \
	if ( $$1 ~ /^sequences/ ) { split($$1,a," "); print "total_sequences:", a[length(a)]; } \
	if ( $$1 ~ /^total length/ ) { split($$1,a," "); print "total_length (bp):", a[3]; } \
	if ( $$1 ~ /^bases masked/ ) { split($$1,a," "); print "total_masquered (bp):", a[3]; \
	print "total_masquered (%):", a[length(a)-1]"%"; \
	exit } \   * avoid last lines *
	}' $< >$@


mask.stat.all: contigs.stat $(FRACTIONS_REPEATS_STATS)
	(for FILE in $(FRACTIONS_REPEATS_STATS); do \
		paste <(echo -e "$$FILE") <(cat <$$FILE | cut -f2 | transpose | cut -f 4); \
	done) \
	| bsort -k 1,1 \
	| cat <(cut -f 1,3 $<) - \
	| sed '1s/^/\t$(notdir $(PWD))\n/' >$@

# ## FOR CORRECTION
# # redo to adapt to position of annotations:
# # RLC_chr9_random:462971-464511 -> chr9_random:462971-464511_RLC
# FRACTIONS_REPEATS_CLASS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.class)
# %.lst.class: %.lst.fasta.ori.out
	# $(call load_modules); \
	# if [ -s $< ]; then \
		# sed 's/^ *//' <$< \
		# | tr -s [:blank:] \\t \
		# | cut -f 10 \
		# | bawk '!/^[$$,\#]/ { \
		# split($$0,a,"_"); \
		# print a[1]; }' >$@; \
	# else \
		# touch $@; \
	# fi;

# # convert random
# %.lst.mask1.corr: %.lst.fasta.ori.out repeats.fasta
	# sed 's/^ *//' <$< \
	# | tr -s [:blank:] \\t \
	# | translate -k \
	# <(fasta2tab <$^2 \
	# | cut -f1 \
	# | grep 'random' \
	# | bawk '{ split($$1,a,"_"); print a[2]"_"a[1], $$1; }') \
	# 10 >$@

# # convert random
# %.lst.mask2.corr: %.lst.fasta.ori.out
	# sed 's/^ *//' <$< \
	# | tr -s [:blank:] \\t \
	# | bawk '!/^[$$,\#]/ { if ( ! ( $$10 ~ /random*/ ) ) { split($$10,a,"_"); print $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9, a[2]"_"a[1], $$11, $$12, $$13, $$14, $$15; } }' >$@


# %.lst.mask.corr: %.lst.mask1.corr %.lst.mask2.corr
	# cat $^ >$@


# %.lst.class.corr: %.lst.class repeats.fasta
	# translate -a -k \
	# <(fasta2tab <$^2 \
	# | cut -f1 \
	# | grep 'random' \
	# | bawk '{ split($$1,a,"_"); print a[2], a[3]; }') \
	# 1 <$< \
	# | cut -f 2 >$@

# %.lst.class.final: %.lst.class.corr %.lst.class
	# cat $< <(grep -v 'random' <$^2) \
	# | bsort \
	# | uniq -c \
	# | sed 's/^ *//' \
	# | tr -s [:blank:] \\t \
	# | select_columns 2 1 >$@
# ## FOR CORRECTION


.PHONY:
test:
	@ echo $(notdir $(PWD))


ALL += reference.fasta \
	repeats.fasta \
	contigs.stat \
	$(FRACTIONS_ORI_OUT) \
	$(FRACTIONS_REPEATS_CLASS) \
	$(FRACTIONS_REPEATS_STATS) \
	mask.stat.all





INTERMEDIATE += 

CLEAN += log \
