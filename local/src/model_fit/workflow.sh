#Run for finding the normal contigs
dir=/projects/novabreed/share/marroni/scripts/logs
cd $dir
MYDIR=/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/
OUTDIR=/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/plots/
infile=(gouais_blanc3_merged.nodup.per_reference_coverage.txt \
kishmish4_merged.nodup.per_reference_coverage.txt \
pn40024_merged.nodup.per_reference_coverage.txt \
rkatsiteli4_merged.nodup.per_reference_coverage.txt \
rpv3_3_merged.nodup.per_reference_coverage.txt \
sangiovese_vcr23_4_merged.nodup.per_reference_coverage.txt \
sultanina1_merged.nodup.per_reference_coverage.txt \
traminer3_merged.nodup.per_reference_coverage.txt)
autofind=(TRUE TRUE FALSE TRUE TRUE TRUE TRUE TRUE)
#infile=(pn40024_merged.nodup.per_reference_coverage.txt)
#autofind=(FALSE)
lengfile=$(echo ${#infile[@]})
for ((i=0;i<${lengfile};i++))
do
for mu in 30 70 140
do
for sigma in 30 60 90
do
for kappa in 150 80 30
do
echo R "--no-save --args status=normal mu=${mu} sigma=$sigma kappa=$kappa infile=${infile[i]} indir=$MYDIR outdir=${OUTDIR} autofind=${autofind[i]} < /projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/workflow.r \
>${dir}/tmp.out 2>${dir}/tmp.err"| qsub -N t_${mu}_${sigma}_${kappa} -l vmem=2G,walltime=4:00:00
done
done
done
done

#Run for finding the hemizygous contigs

dir=/projects/novabreed/share/marroni/scripts/logs
cd $dir
MYDIR=/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/
OUTDIR=/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/plots/
infile=(gouais_blanc3_merged.nodup.per_reference_coverage.txt \
kishmish4_merged.nodup.per_reference_coverage.txt \
pn40024_merged.nodup.per_reference_coverage.txt \
rkatsiteli4_merged.nodup.per_reference_coverage.txt \
rpv3_3_merged.nodup.per_reference_coverage.txt \
sangiovese_vcr23_4_merged.nodup.per_reference_coverage.txt \
sultanina1_merged.nodup.per_reference_coverage.txt \
traminer3_merged.nodup.per_reference_coverage.txt)
autofind=(TRUE TRUE FALSE TRUE TRUE TRUE TRUE TRUE)
# infile=(pn40024_merged.nodup.per_reference_coverage.txt)
# autofind=(FALSE)
lengfile=$(echo ${#infile[@]})
for ((i=0;i<${lengfile};i++))
do
for mu in 30 70 140
do
for sigma in 30 60 90
do
for kappa in 150 80 30
do
echo R "--no-save --args status=hemi mu=${mu} sigma=$sigma kappa=$kappa infile=${infile[i]} indir=$MYDIR outdir=${OUTDIR} autofind=${autofind[i]} < /projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/workflow.r \
>${dir}/tmp.out 2>${dir}/tmp.err"| qsub -N t_${mu}_${sigma}_${kappa} -l vmem=1G,walltime=3:00:00
done
done
done
done



