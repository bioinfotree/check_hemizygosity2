what.to.do=(commandArgs(TRUE))
if(length(what.to.do)>0)
{
	status<-as.character(unlist(strsplit(what.to.do[1],"="))[2])
	mu<-as.numeric(as.character(unlist(strsplit(what.to.do[2],"="))[2]))
	sigma<-as.numeric(as.character(unlist(strsplit(what.to.do[3],"="))[2]))
	kappa<-as.numeric(as.character(unlist(strsplit(what.to.do[4],"="))[2]))
	infile<-as.character(unlist(strsplit(what.to.do[5],"="))[2])
	indir<-as.character(unlist(strsplit(what.to.do[6],"="))[2])
	outdir<-as.character(unlist(strsplit(what.to.do[7],"="))[2])
	autofind<-as.character(unlist(strsplit(what.to.do[8],"="))[2])
	
}

if(status=="normal"|status=="hemi")
{
source("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/hemi_disc.r")
if(status=="hemi")
{
#Run for hemizygous distribution
out.hist=paste(gsub(".txt","",infile),"_hemi_mu_",mu,"_sigma_",sigma,"_kappa_",kappa,".pdf",sep="")
out.hist<-paste(outdir,out.hist,sep="")
out.pav.geno=paste(infile,"_hemi_mu_",mu,"_sigma_",sigma,"_kappa_",kappa,".txt",sep="")
fullfile<-paste(indir,infile,sep="")
study.coverage(infile=fullfile,out.hist=out.hist,out.pav.geno=out.pav.geno,mu.s=mu,sigma.s=sigma,k.s=kappa,autofind=autofind)
}
if(status=="normal")
{
#Run for normal distribution
out.hist=paste(gsub(".txt","",infile),"_norm_mu_",mu,"_sigma_",sigma,"_kappa_",kappa,".pdf",sep="")
out.hist<-paste(outdir,out.hist,sep="")
out.pav.geno=paste(infile,"_norm_mu_",mu,"_sigma_",sigma,"_kappa_",kappa,".txt",sep="")
fullfile<-paste(indir,infile,sep="")
study.coverage(infile=fullfile,out.hist=out.hist,out.pav.geno=out.pav.geno,mu.s=mu,sigma.s=sigma,k.s=kappa,hemi=F,autofind=autofind)
}
}

#The one below is just a reminder of the values to be used to run the analysis
#The appropriate values of mu, sigma and k, have to be identified by hand on the pdfs and the used on the below function.
#There is no way to do this in a really automatic way
if(status="done_by_hand")
{
source("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/hemi_disc.r")
my.sd=1.96
#Gouais blanc
select.contigs.coverage(
infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/gouais_blanc3_merged.nodup.per_reference_coverage.txt",
out.hist=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/gouais_blanc3_merged.nodup.per_reference_coverage_",my.sd,".pdf",sep=""),
out.pav.geno=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/gouais_blanc3_merged.nodup.per_reference_coverage_hemi_",my.sd,".txt",sep=""),
mu.hemi=30,sigma.hemi=30,k.hemi=150,mu.norm=140,sigma.norm=60,k.norm=80,my.sd=my.sd)
#Rkatsiteli
select.contigs.coverage(
infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/rkatsiteli4_merged.nodup.per_reference_coverage.txt",
out.hist=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/rkatsiteli4_merged.nodup.per_reference_coverage_",my.sd,".pdf",sep=""),
out.pav.geno=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/rkatsiteli4_merged.nodup.per_reference_coverage_hemi_",my.sd,".txt",sep=""),
mu.hemi=30,sigma.hemi=30,k.hemi=150,mu.norm=140,sigma.norm=90,k.norm=80,my.sd=my.sd)
#PN40024
select.contigs.coverage(
infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/pn40024_merged.nodup.per_reference_coverage.txt",
out.hist=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/pn40024_merged.nodup.per_reference_coverage_",my.sd,".pdf",sep=""),
out.pav.geno=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/pn40024_merged.nodup.per_reference_coverage_hemi_",my.sd,".txt",sep=""),
mu.hemi=30,sigma.hemi=60,k.hemi=150,mu.norm=140,sigma.norm=90,k.norm=80,autofind=F,my.sd=my.sd)
#sangiovese
select.contigs.coverage(
infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sangiovese_vcr23_4_merged.nodup.per_reference_coverage.txt",
out.hist=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sangiovese_vcr23_4_merged.nodup.per_reference_coverage_",my.sd,".pdf",sep=""),
out.pav.geno=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sangiovese_vcr23_4_merged.nodup.per_reference_coverage_hemi_",my.sd,".txt",sep=""),
mu.hemi=30,sigma.hemi=30,k.hemi=150,mu.norm=140,sigma.norm=90,k.norm=80,my.sd=my.sd)
#sultanina
select.contigs.coverage(
infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sultanina1_merged.nodup.per_reference_coverage.txt",
out.hist=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sultanina1_merged.nodup.per_reference_coverage_",my.sd,".pdf",sep=""),
out.pav.geno=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sultanina1_merged.nodup.per_reference_coverage_hemi_",my.sd,".txt",sep=""),
mu.hemi=30,sigma.hemi=60,k.hemi=80,mu.norm=140,sigma.norm=30,k.norm=80,my.sd=my.sd)
#traminer
select.contigs.coverage(
infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/traminer3_merged.nodup.per_reference_coverage.txt",
out.hist=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/traminer3_merged.nodup.per_reference_coverage_",my.sd,".pdf",sep=""),
out.pav.geno=paste("/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/traminer3_merged.nodup.per_reference_coverage_hemi_",my.sd,".txt",sep=""),
mu.hemi=30,sigma.hemi=60,k.hemi=80,mu.norm=140,sigma.norm=60,k.norm=80,my.sd=my.sd)

}
