#Try to identify the threshold for defining hemizyogus contigs in Michele's assembly (coverage given by contig)



# Possible methods 
# emp.hist (empirical use of histograms do define the classes)
# poisson (use of poisson expectations)
study.coverage<-function(infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sultanina1_merged.nodup.per_reference_coverage.txt",
						out.hist="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sultanina1_merged.nodup.per_reference_coverage.pdf",
						out.pav.geno="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/sultanina1_merged.nodup.per_reference_coverage_hemi.txt",
						required.av.cov=15,min.frac.zero=0.15,one.tail.p=0.10,method="emp.hist",mu.s=1,sigma.s=1,k.s=1,hemi=T,autofind=TRUE)

{
library(data.table)
cat("method used is:",method,"\n")
cat("minimum average coverage is:",required.av.cov,"\n")
cat("minimum fraction of frequence zero is:",min.frac.zero,"\n")
cat("tail correction:",one.tail.p,"\n")
coverage<-fread(infile,data.table=F)


	myline<-coverage[,6]
	myline<-winsorhigh(myline)
	nbreaks<-10*round(summary(myline)[5]/10,0)
	#For very low 
	#cat(summary(normalized[myline,]))
	#cat(nbreaks,"\n")
	#Save the histogram object and remove the last class (which may contain exxageratedly high number of outliers)
	gino<-hist(as.numeric(myline),breaks=nbreaks,plot=F)
	gino$mids<-gino$mids[1:(length(gino$mids)-1)]
	gino$counts<-gino$counts[1:(length(gino$counts)-1)]
	gino$breaks<-gino$breaks[1:(length(gino$breaks)-1)]
	gino$density<-gino$density[1:(length(gino$density)-1)]
	#Compute the mode of the distribution
	#We keep out the last 5 bars (assuming that no value of interest for us is in there) 
	my.mode.place<-which.max(gino$counts)
	my.mode<-gino$mids[my.mode.place]
	my.count<-gino$counts[my.mode.place]
	
	#Explore data to assess if the mode corresponds to hemizyogus contigs or normal contigs
	which.half<-which.min(abs(gino$mids-(my.mode/2)))
	which.twice<-which.min(abs(gino$mids-(2*my.mode)))
	mode.half<-gino$mids[which.half]
	mode.twice<-gino$mids[which.twice]
	count.half<-gino$counts[which.half]
	count.twice<-gino$counts[which.twice]
	# If the difference between the mode and half the mode is lower than that between the mode and twice the mode
	# we can assume that the mode is the one for the hemizyogus contigs and the other mode is twice the mode.
	# Otherwise, the mode is the one for normal contigs and the hemyzigous are half the mode
	# The reasoning is that (after eventually removing outliers at zero or at the maximum), after finding the mode, either twice of it or half of it is the second peak
	# of the distribution and we just have to find which one is it 
	if(autofind==TRUE)
	{
	if(abs(log(my.count)-log(count.half))>=abs(log(my.count)-log(count.twice)))
	{
	mode.het<-my.mode
	mode.hom<-2*mode.het
	} else {
	mode.hom<-my.mode
	mode.het<-round(mode.hom/2,0)
	}
	} 
	if(autofind==FALSE)
	{
	mode.het<-my.mode
	mode.hom<-2*mode.het
	}
	#library(nls2)
	#Try to fit normal distribution to the lower part of the distribution
	#Find the index of the hemizygous mode
	if(hemi)
	{
	hem.ind<-which.min(abs(gino$mids-mode.het))
	my.df<-data.frame(mids=gino$mids,counts=gino$counts)
	hemi.df<-my.df[(hem.ind-10):(hem.ind+10),]
	our.norm <- nls( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=c(mu=mu.s,sigma=sigma.s,k=k.s),data=hemi.df,control=nls.control(minFactor=1/1000000000,maxiter=100,warnOnly=F) )
	#our.norm <- nls2( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=my.start,data=hemi.df,weights=counts,algorithm="brute-force",all=F,maxiter=10000)
	v <- summary(our.norm)$parameters[,"Estimate"]
	hemi.fitted.y<-v[3]*exp(-1/2*(gino$mids-v[1])^2/v[2]^2)
	}
	# regmod<- "counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2)"
	# ones <- c(k = 1, mu = 1, sigma = 1)
	# anmrt <- nlxb(regmod, start = ones, trace = FALSE, data = hemi.df)
	#Try to fit normal distribution to the upper part of the distribution
	if(!hemi)
	{
	norm.ind<-which.min(abs(gino$mids-mode.hom))
	my.df<-data.frame(mids=gino$mids,counts=gino$counts)
	norm.df<-my.df[(norm.ind-10):(norm.ind+10),]
	our.norm <- nls( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=c(mu=mu.s,sigma=sigma.s,k=k.s),data=norm.df,control=nls.control(minFactor=1/1000000000,warnOnly=F) )
#	our.norm <- nls( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=c(mu=50,sigma=50,k=25),data=norm.df,control=nls.control(minFactor=1/1000000000,maxiter=100,warnOnly=T) )
	v <- summary(our.norm)$parameters[,"Estimate"]
	normal.fitted.y<-v[3]*exp(-1/2*(gino$mids-v[1])^2/v[2]^2)
	}
	
	
	
	#Define border of region "shadow1" i.e. the region between thos surely AA and those surely AP
	shad1<-mode.het/3
	shad2<-mode.het+0.5*mode.het
	
	
	outpdf<-gsub("check",paste("check_",method,"_mincov",required.av.cov,"_minzero",min.frac.zero,"_tail",one.tail.p,sep=""),out.hist)
	pdf(outpdf)
	
	#Plot the histogram
	hist(as.numeric(myline),breaks=nbreaks, main="Metti il titolo",xlab="normalized coverage")
	if(hemi) lines(gino$mids,hemi.fitted.y,col="green") else lines(gino$mids,normal.fitted.y,col="brown")

	
	mtext(c("AA","shad1","AP","shad2","PP"),side=1,at=c(0,(1+shad1)/2,mode.het,(shad2+mode.hom)/2,mode.hom),cex=0.7,
			col=c("black","grey","blue","grey","red"))
	#Grey is shad1
	segments(1,1,shad1,1,col="Grey",lw=2)
	#Blue is AP
	segments(shad1,1,shad2,1,col="blue",lw=2)
	#Grey is shad2
	segments(shad2,1,mode.hom,1,col="grey",lw=2)
	#Red is PP
	segments(mode.hom,1,max(gino$mids),col="red",lw=2)
	
	#Plot expected Poisson distributions
	# rpois: random generation for the Poisson distribution with parameter 'lambda'
	dhet<-density(rpois(100000,mode.het))
	#Scale the density (using the max) so that the max density poisson is equal to the mode
	scale.factor.het<-gino$counts[gino$mids==mode.het]/max(dhet$y)
	lines(dhet$x,scale.factor.het*dhet$y,col="blue",xlab=F)
	dhom<-density(rpois(100000,mode.hom))
	scale.factor.hom<-gino$counts[which.min(abs(gino$mids-mode.hom))]/max(dhom$y)
	lines(dhom$x,scale.factor.hom*dhom$y,col="red",xlab=F)
	# ppois: distribution function for the Poisson distribution with parameter 'lambda'. 
	# ppois(q=number of events-corresponding to coverage classes, lambda=mode.het ("ideal" value of coverage for het)). p.het is the probability to have that level of coverage, given the "ideal" average
	p.het.low<-ppois(1:mode.het,mode.het)
	p.het.hi<-1-ppois((mode.het+1):max(gino$mids),mode.het)
	# Lower Confidential Level (is the coverage class corresponding to the highest probability for the distribution in the confidence interval of 90%)
	LCL.het<-which.min(abs(p.het.low-one.tail.p))
	# Upper Confidential Level
	UCL.het<-mode.het+which.min(abs(p.het.hi-one.tail.p))
	p.hom.low<-ppois(1:mode.hom,mode.hom)
	p.hom.hi<-1-ppois((mode.hom+1):max(gino$mids),mode.hom)
	LCL.hom<-which.min(abs(p.hom.low-one.tail.p))
	UCL.hom<-mode.hom+which.min(abs(p.hom.hi-one.tail.p))
	legend("topright",c(paste("shad1=",shad1),paste("AP=",mode.het),paste("shad2=",shad2),paste("PP=",mode.hom),paste("LCL.het=",LCL.het),paste("UCL.het=",UCL.het),paste("LCL.hom=",LCL.hom),paste("UCL.hom=",UCL.hom)))

dev.off()
}



select.contigs.coverage<-function(infile="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/gouais_blanc.txt",
						out.hist="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/gouais_blanc.pdf",
						out.pav.geno="/projects/novabreed/share/mvidotto/doc/prj/novabreed/genome_assembly_evaluation/read_alignments/paired_end/test_discrimination_hemi/gouais_blanc_hemi.txt",
						one.tail.p=0.10,mu.hemi=1,sigma.hemi=1,k.hemi=1,mu.norm=1,sigma.norm=1,k.norm=1,autofind=T,my.sd=1.96)

{
library(data.table)
coverage<-fread(infile,data.table=F)


	myline<-coverage[,6]
	myline<-winsorhigh(myline)
	nbreaks<-10*round(summary(myline)[5]/10,0)
	#Save the histogram object and remove the last class (which may contain exxageratedly high number of outliers)
	gino<-hist(as.numeric(myline),breaks=nbreaks,plot=F)
	gino$mids<-gino$mids[1:(length(gino$mids)-1)]
	gino$counts<-gino$counts[1:(length(gino$counts)-1)]
	gino$breaks<-gino$breaks[1:(length(gino$breaks)-1)]
	gino$density<-gino$density[1:(length(gino$density)-1)]
	#Compute the mode of the distribution
	#We keep out the last 5 bars (assuming that no value of interest for us is in there) 
	my.mode.place<-which.max(gino$counts)
	my.mode<-gino$mids[my.mode.place]
	my.count<-gino$counts[my.mode.place]
	
	#Explore data to assess if the mode corresponds to hemizyogus contigs or normal contigs
	which.half<-which.min(abs(gino$mids-(my.mode/2)))
	which.twice<-which.min(abs(gino$mids-(2*my.mode)))
	mode.half<-gino$mids[which.half]
	mode.twice<-gino$mids[which.twice]
	count.half<-gino$counts[which.half]
	count.twice<-gino$counts[which.twice]
	# If the difference between the mode and half the mode is lower than that between the mode and twice the mode
	# we can assume that the mode is the one for the hemizyogus contigs and the other mode is twice the mode.
	# Otherwise, the mode is the one for normal contigs and the hemyzigous are half the mode
	# The reasoning is that (after eventually removing outliers at zero or at the maximum), after finding the mode, either twice of it or half of it is the second peak
	# of the distribution and we just have to find which one is it 
	if(autofind==TRUE)
	{
	if(abs(log(my.count)-log(count.half))>=abs(log(my.count)-log(count.twice)))
	{
	mode.het<-my.mode
	mode.hom<-2*mode.het
	} else {
	mode.hom<-my.mode
	mode.het<-round(mode.hom/2,0)
	}
	} 
	if(autofind==FALSE)
	{
	mode.het<-my.mode
	mode.hom<-2*mode.het
	}

	
	
	
	hem.ind<-which.min(abs(gino$mids-mode.het))
	my.df<-data.frame(mids=gino$mids,counts=gino$counts)
	hemi.df<-my.df[(hem.ind-10):(hem.ind+10),]
	our.norm <- nls( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=c(mu=mu.hemi,sigma=sigma.hemi,k=k.hemi),data=hemi.df,control=nls.control(minFactor=1/1000000000,maxiter=100,warnOnly=F) )
	#our.norm <- nls2( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=my.start,data=hemi.df,weights=counts,algorithm="brute-force",all=F,maxiter=10000)
	vh <- summary(our.norm)$parameters[,"Estimate"]
	hemi.fitted.y<-vh[3]*exp(-1/2*(gino$mids-vh[1])^2/vh[2]^2)

	norm.ind<-which.min(abs(gino$mids-mode.hom))
	my.df<-data.frame(mids=gino$mids,counts=gino$counts)
	norm.df<-my.df[(norm.ind-10):(norm.ind+10),]
	our.norm <- nls( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=c(mu=mu.norm,sigma=sigma.norm,k=k.norm),data=norm.df,control=nls.control(minFactor=1/1000000000,warnOnly=F) )
#	our.norm <- nls( counts ~ k*exp(-1/2*(mids-mu)^2/sigma^2), start=c(mu=50,sigma=50,k=25),data=norm.df,control=nls.control(minFactor=1/1000000000,maxiter=100,warnOnly=T) )
	vn <- summary(our.norm)$parameters[,"Estimate"]
	normal.fitted.y<-vn[3]*exp(-1/2*(gino$mids-vn[1])^2/vn[2]^2)
	pdf(out.hist)
	
	#Plot the histogram
	hist(as.numeric(myline),breaks=nbreaks, main="Metti il titolo",xlab="normalized coverage")
	lines(gino$mids,hemi.fitted.y,col="blue") 
	lines(gino$mids,normal.fitted.y,col="red")

	LCL.het<-round(vh[1]-my.sd*vh[2],3)
	UCL.het<-round(vh[1]+my.sd*vh[2],3)
	LCL.nor<-round(vn[1]-my.sd*vn[2],3)
	UCL.nor<-round(vn[1]+my.sd*vn[2],3)
	
	legend("topright",c(paste("LCL.het=",LCL.het),paste("UCL.het=",UCL.het),paste("LCL.hom=",LCL.nor),paste("UCL.hom=",UCL.nor)))
	
	coverage$zygosity<-"."
	
	coverage$zygosity[myline<1]<-"No_cov"
	coverage$zygosity[myline<UCL.het&myline<LCL.nor]<-"Hemi"
	coverage$zygosity[myline>=UCL.het&myline>=LCL.nor]<-"Normal"

dev.off()
write.table(coverage,out.pav.geno,quote=F,row.names=F,sep="\t")
}





winsorhigh<-function (x, fraction=.05)
{
   if(length(fraction) != 1 || fraction < 0 ||
         fraction > 0.5) {
      stop("bad value for 'fraction'")
   }
   lim <- quantile(x, probs=c(fraction, 1-fraction))
   x[ x > lim[2] ] <- lim[2]
   x
}


round_df <- function(df, digits) 
{
	nums <- vapply(df, is.numeric, FUN.VALUE = logical(1))
	df[,nums] <- round(df[,nums], digits = digits)
	(df)
}





